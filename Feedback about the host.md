- Jump to «ENGLISH».
- Saltá a «ESPAÑOL».
- Vá ao «PORTUGUÊS».

[ENGLISH]

I liked the host's flat because it is a very unusual place, with many unfamiliar things, despite my family. The host was very friendly and helpful as his brother as well. The location is close to many pharmacies, shopping centres, supermarkets, restaurants, and bars. The metro station is also very close. Tourists, if you are unfamiliar with the suite bath, I recommend you to call the host's brother before doing such sh*t, but be friendly with him, please. Moreover, please obey the host's rules. I will give you some tips on how you will get well treated and safe, as an experienced and professional tourist, being deaf and autistic, here are:

- Please always speak in Spanish, be humble and polite, and say «good day», «pardon» and «thank you» in Spanish, and the Argentines will treat you well and love you. 
- Remember that nowhere in Buenos Aires accepts the credit card of a foreigner, so you have to use Western Union and go to the shopping centre El Solar to get the Argentine money. If the money papers are too many, please carry a chest bag.
- Please avoid using Uber! The Uber is not regulated yet, and the Uber taxis are known for kidnapping and robbing the tourists. Use the bondis, the underground or the regulated taxis.
- Please respect the etiquette rules, as do not block the people's walking in the street, do not speak loudly, and do not cut the queue. The Argentines do not like that. 
- Please, in your last trip day, wash the dishes, clean the floor, and tidy up the beds and bedrooms. The host and the cleaning lady will be very grateful to you.
- Bring some souvenirs from your country in your first trip day and leave them in the flat for the host, his brother and the cleaning lady, at the last trip day.

Anyway, I loved Buenos Aires, and I will come back to Buenos Aires and his apartment again, but without my family.

[ESPAÑOL]

Me gustó el departamento del anfitrión, porque es un lugar muy inusual, con muchas cosas desconocidas, a pesar de mi familia. El anfitrión fue muy amable y servicial, como su hermano. La ubicación está cerca de muchas farmacias, centros comerciales, supermercados, restaurantes y bares. La estación de metro también está muy cerca. Turistas, si vos no estás familiarizado con el baño de la suite, te recomiendo que llamá al hermano del anfitrión antes de hacer tal mi*rda, pero sé amable con él, por favor. Además, por favor obedecé las reglas del anfitrión. Te daré algunos consejos sobre cómo serás bien tratado y seguro, como turista experimentado y profesional, sordo y autista, aquí están:

- Por favor, siempre hablá en español, sé humilde y educado, y decí «buen día», «perdón» y «gracias» en español, y los argentinos te tratarán bien y te amarán.
- Recordá que en ningún lugar de Buenos Aires aceptan la tarjeta de crédito de un extranjero, por eso, tenés que usar Western Union e ir al centro comercial El Solar para obtener el dinero argentino. Si los papeles de dinero son demasiados, por favor, llevá un bolso de pecho.
- ¡Por favor, evitá usar Uber! El Uber no está regulado todavía, y los taxis de Uber son conocidos por secuestrar y robar a los turistas. Usá los bondis, el subte o los taxis regulados.
- Por favor, respetá las reglas de etiqueta, como no bloquear el paso de la gente en la calle, no hablar en voz alta y no cortar la cola. A los argentinos no les gusta eso.
- Por favor, en tu último día de viaje, lavá los platos, limpiá el piso y ordená las camas y los dormitorios. El anfitrión y la chica de la limpieza estarán muy agradecidos con vos.
- Traé algunos recuerdos de tu país en tu primer día de viaje y dejalos en el departamento para el anfitrión, su hermano y la chica de la limpieza, en el último día de viaje.

De todos modos, me gustó Buenos Aires, y volveré a Buenos Aires y a su departamento, pero sen mi familia.

[PORTUGUÊS]

Gostei do apartamento do anfitrião, porque é um lugar muito incomum, com muitas coisas desconhecidas, apesar da minha família. O anfitrião foi muito simpático e prestativo, assim como seu irmão. A localização está perto de muitas farmácias, centros comerciais, supermercados, restaurantes e bares. A estação de metrô também está muito perto. Turistas, se você não está familiarizado com o banho da suíte, recomendo que chame o irmão do anfitrião antes de fazer tal m*rda, mas seja gentil com ele, por favor. Além disso, por favor, obedeça as regras do anfitrião. Darei algumas dicas de como será bem tratado e seguro, como turista experiente e profissional, surdo e autista, aqui estão:

- Por favor, sempre fale em espanhol, seja humilde e educado, e diga «buen día», «perdón» e «gracias» em espanhol, e os argentinos te tratarão bem e te amarão.
- Lembre-se de que, em nenhum lugar de Buenos Aires aceitam o cartão de crédito de um estrangeiro, por isso, você tem de usar Western Union e ir ao centro comercial El Solar para pegar o dinheiro argentino. Se os papéis de dinheiro forem muitos, por favor, leve uma bolsa de peito.
- Por favor, evite usar Uber! O Uber não está regulamentado ainda, e os táxis do Uber são conhecidos por sequestrar e roubar os turistas. Use os bondis, o metrô ou os táxis regulamentados.
- Por favor, respeite as regras de etiqueta, como não bloquear a passagem das pessoas na rua, não falar alto e não cortar a fila. Os argentinos não gostam disso.
- Por favor, no seu último dia de viagem, lave os pratos, limpe o chão e arrume as camas e os quartos. O anfitrião e a moça da limpeza ficarão muito gratos a você.
- Traga algumas lembranças do seu país no seu primeiro dia de viagem e deixe-as no apartamento para o anfitrião, seu irmão e a moça da limpeza, no último dia de viagem.

De qualquer forma, gostei de Buenos Aires, e voltarei a Buenos Aires e ao seu apartamento, mas sem a minha família.